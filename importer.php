<?php
require('vendor/autoload.php');

class Importer
{
    private $data;

    private $customers;

    private $newCustomers;

    private $url;

    private $headers;

    private $client;

    private $fileOneName;

    private $fileTwoName;

    private $delimiter;

    public function __construct()
    {
        $this->delimiter = ';';

        $this->fileOneName = __DIR__ . '/data/customer-addressess-' . time() . '.txt';
        $this->fileTwoName = __DIR__ . '/data/customer-company-oib-' . time() . '.txt';

        $this->url = 'http://vrutakshop.local/api/customers';
        $this->headers = [
            'Postman-Token' => '85f3e46a-63a9-49ad-a761-0bfd5b6c9d87',
            'cache-control' => 'no-cache',
            'Authorization' => 'Bearer dBffyLR8tFo-zzXTUfGtw6dK7eAqmI2axV9Rwbtp7e7FSKtRrAb19Kq0NTtbG5epPog5nDLGoQFIV9QkmIIsmUNTaDQ2NKGTqNDN7SKSr3R7KAiyT5-1l1IAV5u-pBpshU-BJoTs_zOhzGkWAlfAPUJQXnGT5FL5gS3efgYkGbYpdskyoW0lCEptn4ssVnMpQt-PYP1NPg76AublJ3aFIrFycTH8J7hwYseqiOEaeu3IoSQIVp_qo2w6fqDqGTh8pN-BkhLBYwq6y0Ln-lcIJA'
        ];

        $this->client = new \GuzzleHttp\Client();
    }

    public function run()
    {
        $this->loadData();
        $this->parseData();
        $this->createNewStructure();
        $this->submitUsersToApi();
    }

    public function submitUsersToApi()
    {
        foreach ($this->newCustomers as $key => $customer) {
            $this->createUser($customer);
        }
    }

    /**
     * @param array $customer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createUser(array $customer)
    {
        $response = $this->client->request('POST', $this->url, [
            GuzzleHttp\RequestOptions::JSON => $customer,
            'headers' => $this->headers
        ]);

        if($response->getStatusCode() != 200) {
            echo(sprintf('User %s failed!', $customer['customer']['email']) . PHP_EOL);
        }

        $responseObj = json_decode($response->getBody()->getContents());

        $userObj = $responseObj->customers[0];
        
        $userId = $userObj->id;
        $billingAddrId = $userObj->billing_address->id;
        $billingAddrCompany = $userObj->billing_address->company;
        $shippingAddrId = $userObj->shipping_address->id;

        $fileOneLine = join($this->delimiter, [$userId, $billingAddrId, $shippingAddrId]);
        $fileTwoLine = join($this->delimiter, [$userId, $billingAddrCompany, $customer['OIB']]);

        file_put_contents($this->fileOneName, $fileOneLine . PHP_EOL, FILE_APPEND);
        file_put_contents($this->fileTwoName, $fileTwoLine . PHP_EOL, FILE_APPEND);

        echo(sprintf('User %s created', $customer['customer']['email']) . PHP_EOL);

    }

    public function createNewStructure()
    {
        foreach ($this->customers as $customer) {
            $this->remapArrayStructure($customer);
        }
    }

    protected function createCustomerArrayFromCsv(array $data)
    {

        foreach ($data as $key => $value) {
            $data[$key] = $this->removeWeirdSyntax($value);
        }

        $customer['customerID'] = $data[0];
        $customer['companyName'] = $data[1];
        $customer['companyUID'] = $data[2];
        $customer['firstName'] = $data[3];
        $customer['lastName'] = $data[4];
        $customer['address'] = $data[5];
        $customer['city'] = $data[6];
        $customer['ZIP'] = $data[7];
        $customer['country'] = $data[8];
        $customer['email'] = $data[9];
        $customer['phone'] = $data[10];
        $customer['password'] = $data[11];
        $customer['isActive'] = $data[12];
        $customer['isWishlistPublic'] = $data[13];
        $customer['dateCreated'] = $data[14];
        $customer['lastLoginDate'] = $data[15];
        $customer['gender'] = $data[16];
        $customer['dateOfBirth'] = $data[17];
        $customer['profile'] = $data[18];

        return $customer;
    }

    public function remapArrayStructure($customer)
    {

        $newCustomer = [
            "OIB" => $customer["companyUID"],
            "customer" => [
                "first_name" => $customer['firstName'],
                "last_name" => $customer['lastName'],
                "email" => $customer['email'],
                "password" => $customer['password'],
                "adminComment" => "", // kaj tu?
                "role_ids" => [3],
                "billing_address" => [
                    "first_name" => $customer['firstName'],
                    "last_name" => $customer['lastName'],
                    "email" => $customer['email'],
                    "company" => $customer['companyName'],
                    "country_id" => 24,
                    "country" => $customer['country'],
                    "city" => $customer['city'],
                    "address1" => $customer['address'],
                    "address2" => "",
                    "phone_number" => $customer['phone'],
                    "zip_postal_code" => $customer['ZIP'],
                    "fax_number" => "",
                    "customer_attributes" => '<Attributes><AddressAttribute ID="1"><AddressAttributeValue><Value>' . $customer["companyUID"] . '</Value></AddressAttributeValue></AddressAttribute></Attributes>'
                ],
                "shipping_address" => [
                    "first_name" => $customer['firstName'],
                    "last_name" => $customer['lastName'],
                    "email" => $customer['email'],
                    "company" => $customer['companyName'],
                    "country_id" => 24,
                    "country" => $customer['country'],
                    "city" => $customer['city'],
                    "address1" => $customer['address'],
                    "address2" => "",
                    "phone_number" => $customer['phone'],
                    "zip_postal_code" => $customer['ZIP'],
                    "fax_number" => ""
                ]
            ]
        ];

        $this->newCustomers[] = $newCustomer;
    }

    public function loadData()
    {
        $this->data = array_map('str_getcsv', file('data/TestCustomer.csv'));
    }

    public function parseData()
    {
        foreach ($this->data as $customerData) {
            $this->customers[] = $this->createCustomerArrayFromCsv($customerData);
        }
    }

    public function removeWeirdSyntax($value)
    {
        $value = trim($value);
        $value = str_replace('\'', '', $value);

        return $value === '' ? 'null' : $value;
    }
}


(new Importer())->run();
?>